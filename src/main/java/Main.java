
public class Main implements Discord.Bot {

    /**
     *                         HOW TO USE
     * =========================================================
     * Go to the following url:
     * https://github.com/reactiflux/discord-irc/wiki/Creating-a-discord-bot-&-getting-a-token
     *
     * Follow the steps, place your token down in the variable 'TOKEN'
     * Run the application and type '!hello' in your server
     */

    public static final String TOKEN = "";

    public static void main(String[] args) {
        Discord.start(TOKEN);
    }

    @Override
    public void onMessage() {
        if(Discord.getCommand().equals("hello")){
            Discord.sendMessage("world!");
        }
    }
}
