import org.javacord.api.DiscordApi;
import org.javacord.api.DiscordApiBuilder;
import org.javacord.api.entity.channel.PrivateChannel;
import org.javacord.api.entity.channel.TextChannel;
import org.javacord.api.event.message.MessageCreateEvent;

import java.util.Optional;
import java.util.concurrent.CompletionException;

public class Discord {

    private static DiscordApi api = null;
    private static Bot context;
    private static MessageCreateEvent lastMessageCreateEvent = null;
    private static String listenerString = "!";

    /**
     * Call this method in your main method by typing Discord.start("Your token here")
     */
    public static void start(String token){

        try {
            if(api != null) throw new Exception("Discord already started. Don't call Discord.start() more then once.");
            StackTraceElement[] stackTraceElements = Thread.currentThread().getStackTrace();
            Discord.context = (Bot) Class.forName(stackTraceElements[2].getClassName()).newInstance();
            api = new DiscordApiBuilder().setToken(token).login().join();
            api.addMessageCreateListener(event -> {

                try {
                    if (isCommand(event) && !isSelf(event)) {

                        setMessage(event);

                        System.out.println();
                        System.out.println("Message arrived:");
                        System.out.println("- User:     " + getUsername() + " | " + getUserID());
                        System.out.println("- Channel:  " + getChannel().getIdAsString());
                        System.out.println("- Message:  " + getMessage());

                        Discord.context.onMessage();
                    }
                }
                catch (Exception e){
                    e.printStackTrace();
                    System.exit(7);
                }
                finally {
                    resetMessage();
                }
            });

        } catch (InstantiationException | IllegalAccessException e){
            System.err.println("Do not define a constructor!");
            System.exit(1);
        } catch (CompletionException e){
            System.err.println("Your token is invalid or you have no internet connection");
            System.exit(2);
        } catch (ClassNotFoundException e){
            System.err.println("something went wrong");
            System.exit(3);
        } catch (ClassCastException e){
            System.err.println("Make your class implement Discord.Bot");
            System.exit(4);
        } catch (Exception e){
            System.err.println(e.getMessage());
            System.exit(5);
        }
    }

    /**
     * Checks whether the provided event is meant for the bot
     * @param event The triggered event
     * @return true if the event starts with listenerString
     */
    private static boolean isCommand(MessageCreateEvent event){
        return event.getMessage().getContent().startsWith(listenerString);
    }

    /**
     * Checks whether the message is sent by the bot itself.
     * @param event The triggered event
     * @return true if this bot is the sender
     */
    private static boolean isSelf(MessageCreateEvent event){
        return event.getMessage().getAuthor().getId() == api.getClientId();
    }

    /**
     * Gets the first String of the message
     * @return The command used
     */
    public static String getCommand(){
        return getMessage().split(" ")[0];
    }

    /**
     * Get all the remaining arguments sent
     * @return The rest of the arguments
     */
    public static String[] getArguments(){
        String[] args = getMessage().split(" ");
        String[] args2 = new String[args.length - 1];
        for(int i = 1; i < args.length; i++){
            args2[i - 1] = args[i];
        }
        return args2;
    }

    /**
     * Get a single argument from the message or an empty String if it doesn exist
     * @param n The n'th argument.
     * @return The argument or an empty String
     */
    public static String getArgument(int n){
        String[] args = getArguments();
        if(args.length < n){
            return "";
        }
        return args[n];
    }

    /**
     * Use this metod to send a message to the last channel from which a message was received
     * @param message The message to be sent.
     */
    public static void sendMessage(String message){
        if(lastMessageCreateEvent == null){
            System.err.println("No channel yet to reply to");
            return;
        }
        lastMessageCreateEvent.getChannel().sendMessage(message);
    }

    /**
     * Use this method if you feel confident in implementing al sorts of crazy stuff
     * @return The Discord bot that was created when Discord.start() was called.
     */
    public static DiscordApi getApi(){
        return api;
    }

    /**
     * Set the listener to something you want the bot to listen to. Default is "!"
     * @param listenerString the listener String
     */
    public static void setListenerString(String listenerString) {
        Discord.listenerString = listenerString;
    }

    /**
     * Get the String the app listens on when messages are sent
     * @return The listener String
     */
    public static String getListenerString() {
        return listenerString;
    }

    /**
     * Set the message fields to match the last one
     * @param event
     */
    private static void setMessage(MessageCreateEvent event){
        lastMessageCreateEvent = event;
    }

    /**
     * Gets the last sent message.
     * @return The last sent message.
     */
    public static String getMessage(){
        if(lastMessageCreateEvent == null){
            System.err.println("Only call 'getMessage()' while the 'onMessage()' method is running!");
            System.exit(6);
        }
        String message = lastMessageCreateEvent.getMessage().getContent();
        return message.substring(listenerString.length(), message.length());
    }

    /**
     * Gets the last used channel
     * @return The last used channel
     */
    public static TextChannel getChannel(){
        if(lastMessageCreateEvent == null){
            System.err.println("Only call 'getChannel()' while the 'onMessage()' method is running!");
            System.exit(6);
        }
        return lastMessageCreateEvent.getChannel();
    }

    /**
     * Gets the private channel of the last user
     * @return
     */
    public static PrivateChannel getPrivateChannel(){
        if(lastMessageCreateEvent == null){
            System.err.println("Only call 'getPrivateChannel()' while the 'onMessage()' method is running!");
            System.exit(6);
        }
        Optional<PrivateChannel> pc = lastMessageCreateEvent.getPrivateChannel();
        return pc.orElse(null);
    }

    /**
     * Get the user id from the last user
     * @return
     */
    public static long getUserID(){
        if(lastMessageCreateEvent == null){
            System.err.println("Only call 'getUserID()' while the 'onMessage()' method is running!");
            System.exit(6);
        }
        return lastMessageCreateEvent.getMessage().getAuthor().getId();
    }

    /**
     * Get the username from the last user
     * @return
     */
    public static String getUsername(){
        if(lastMessageCreateEvent == null){
            System.err.println("Only call 'getUsername()' while the 'onMessage()' method is running!");
            System.exit(6);
        }
        return lastMessageCreateEvent.getMessage().getAuthor().getName();
    }

    /**
     * Reset the message fields so there can't be called upon anymore
     */
    private static void resetMessage(){
        lastMessageCreateEvent = null;
    }

    /**
     * Just something to help the developer reminding how to program
     * @param args
     */
    public static void main(String[] args){
        System.err.println("Create your own 'main' method!");
    }

    public interface Bot{
        /**
         * This runs when a message starting with the listener String (default "!") is sent
         */
        void onMessage();
    }
}
